package ru.t1.artamonov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
